clean:
	@@find . -name __pycache__ | xargs rm -rf
	@@find . -name '*.pyc' | xargs rm

gentags:
	ctags --extra=+f -R .

coverage:
	YARGS_ENV='test' py.test --cov-config .coveragerc --cov=. .

style:
	@@pycodestyle --max-line-length 100 --ignore=E731 *.py

autopep8:
	autopep8 -i --max-line-length 100 *.py

autoflake:
	autoflake -i -r --remove-all-unused-imports *.py

sdist:
	rm -rf yargs.egg-info  dist && python setup.py sdist
