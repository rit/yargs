import os
import pytest

from ..yargs import OptionMapper
from ..yargs import yaml2options
from ..yargs import generate_parser


HERE = os.path.dirname(__file__)


def test_init_values(mapper):
    assert mapper._basedir == 'yargs/tests/demotest'
    assert mapper._build_type == 'test'


def test_mapper_value_boolean(mapper):
    settings = mapper.parse([])
    assert settings.debug is False


def test_mapper_value_number(mapper):
    settings = mapper.parse([])
    assert settings.port == 4000

    settings = mapper.parse("--port 5000".split())
    assert settings.port == 5000


def test_mapper_value_number_type_check(mapper):
    with pytest.raises(SystemExit) as e:
        settings = mapper.parse("--port abc".split())


def test_mapper_value_string(mapper):
    settings = mapper.parse([])
    assert settings.static_path == "/static"

    settings = mapper.parse('--static_path /asset'.split())
    assert settings.static_path == "/asset"


def test_mapper_value_choices(mapper):
    settings = mapper.parse([])
    assert settings.logging == "info"

    settings = mapper.parse('--logging info'.split())
    assert settings.logging == "info"


def test_mapper_value_choices_with_invalid_choice(mapper):
    with pytest.raises(SystemExit):
        mapper.parse('--logging foobar'.split())


def test_mapper_value_list_static_hosts(mapper):
    settings = mapper.parse([])
    assert settings.static_hosts == ['s1.example.com', 's2.example.com']

    settings = mapper.parse(["--static_hosts", "foo.com", "bar.com"])
    assert settings.static_hosts == ['foo.com', 'bar.com']

    with pytest.raises(SystemExit) as excinfo:
        mapper.parse("--static_hosts ".split())


def test_mapper_overwrite(mapper):
    settings = mapper.parse([])
    assert settings.nsurl == '/test'


def test_mapper_overwrite_local(tmpdir):
    yml = """
---
nsurl: "/local"
    """
    base = tmpdir.mkdir('base_yml_test')
    path = base.join('app_namespace.test.local.yml')
    path.write(yml)
    mapper = OptionMapper('app_namespace', basedir=str(base))
    settings = mapper.parse([])
    assert settings.nsurl == '/local'


def test_mapper_overwrite_value_rawtext(mapper):
    settings = mapper.parse([])
    assert settings.cookie_secret == 'super-secret'


def test_yaml2options():
    path = os.path.join(HERE, "./demotest/options.yml")
    options = yaml2options(path)
    assert options['debug']['value'] is False
    assert options['debug']['help'] == "Enable debug"
    assert options['nsurl']['value'] == "/"
    assert options['login_url']['value'] == "/signin"


def test_yaml2options_rawtext():
    path = os.path.join(os.path.dirname(__file__), "demotest", "options.test.yml")
    options = yaml2options(path)
    assert options['cookie_secret']['value'] == 'super-secret'


@pytest.fixture
def parser():
    path = os.path.join(HERE, "./demotest/options.yml")
    options = yaml2options(path)
    return generate_parser(options)


@pytest.fixture
def mapper():
    mapper = OptionMapper('options', basedir='yargs/tests/demotest')
    return mapper


@pytest.fixture
def settings(parser):
    return parser.parse_args([])
